module bitbucket.org/pcastools/flag

go 1.13

require (
	github.com/fatih/color v1.13.0
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/go-wordwrap v1.0.1
	golang.org/x/crypto v0.0.0-20220826181053-bd7e27e6170d
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	golang.org/x/term v0.0.0-20220722155259-a9ba230a4035 // indirect
)
