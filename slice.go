// Slice defines functions for working with slices of flags and flag sets.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"strings"
)

/////////////////////////////////////////////////////////////////////////
// flagSlice functions
/////////////////////////////////////////////////////////////////////////

// flagSlice implements sort.Interface for slices of Flags, sorting lexicographically on f.Name().
type flagSlice []Flag

// Len is the number of elements.
func (fs flagSlice) Len() int {
	return len(fs)
}

// Less reports whether the element with index i should sort before the element with index j.
func (fs flagSlice) Less(i, j int) bool {
	n1, n2 := strings.ToLower(fs[i].Name()), strings.ToLower(fs[j].Name())
	if n1 < n2 {
		return true
	} else if n1 == n2 {
		return fs[i].Name() < fs[j].Name()
	}
	return false
}

// Swap swaps the elements with indexes i and j.
func (fs flagSlice) Swap(i, j int) {
	fs[i], fs[j] = fs[j], fs[i]
}

/////////////////////////////////////////////////////////////////////////
// setSlice functions
/////////////////////////////////////////////////////////////////////////

// setSlice implements sort.Interface for slices of Sets, sorting lexicographically on S.Name().
type setSlice []Set

// Len is the number of elements.
func (S setSlice) Len() int {
	return len(S)
}

// Less reports whether the element with index i should sort before the element with index j.
func (S setSlice) Less(i, j int) bool {
	return S[i].Name() < S[j].Name()
}

// Swap swaps the elements with indexes i and j.
func (S setSlice) Swap(i, j int) {
	S[i], S[j] = S[j], S[i]
}
