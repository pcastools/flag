// Flags defines an interface representing flags, and basic types that satisfy that interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"sync"
)

// Flag is the interface a flag satisfies.
//
// A Flag can optionally satisfy the interface
//	type isBooleaner struct {
//		// IsBoolean returns true if and only if this flag is a boolean
//		flag in the sense of the flag package.
//		IsBoolean() bool
//	}
// to indicate whether or not it is a boolean flag in the sense of the flag
// package.
//
// A Flag can optionally satisfy the interface
//	type alternativeNamer interface {
//		// AlternativeName returns an alternative flag name (without leading
//		// hyphen).
//		AlternativeName() string
//	}
// to provide an additional flag name (without leading hyphen) to Name.
type Flag interface {
	Name() string        // Name returns the flag name (without leading hyphen).
	Description() string // Description returns a one-line description of this variable.
	Usage() string       // Usage returns long-form usage text (or the empty string if not required).
	Parse(string) error  // Parse parses the string.
}

// isBooleaner is the interface satisfied by a flag with an IsBoolean method.
type isBooleaner interface {
	IsBoolean() bool
}

// alternativeNamer is the interface satisfied by a flag with an alternative name.
type alternativeNamer interface {
	AlternativeName() string
}

// init registers the standard flag set
func init() {
	standardSet.Add(helpFlag{})
	AddSet(standardSet)
}

/////////////////////////////////////////////////////////////////////////
// the standard flag set
/////////////////////////////////////////////////////////////////////////

var standardSet = NewBasicSet("")

// Add adds the given flags to the standard flag set.
func Add(flags ...Flag) {
	standardSet.Add(flags...)
}

// SetUsageFooter sets the footer in the usage message for the standard flag set.
func SetUsageFooter(s string) {
	standardSet.SetUsageFooter(s)
}

// SetUsageHeader sets the header in the usage message for the standard flag set.
func SetUsageHeader(s string) {
	standardSet.SetUsageHeader(s)
}

// SetName sets the name of the standard flag set.
func SetName(name string) {
	standardSet.m.Lock()
	defer standardSet.m.Unlock()
	standardSet.name = name
}

// SetValidate sets the validation function for the standard flag set
func SetValidate(f func() error) {
	standardSet.SetValidate(f)
}

/////////////////////////////////////////////////////////////////////////
// registeredSets functions
/////////////////////////////////////////////////////////////////////////

// a mutex protecting the following data
var m sync.Mutex

// registeredSets holds the registered flag sets
var registeredSets = setSlice{}

// the global header and footer for the usage string
var globalHeader string
var globalFooter string
