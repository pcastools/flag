// Path defines a Flag that can be used to represent a file's path.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
)

/////////////////////////////////////////////////////////////////////////
// pathFlag functions
/////////////////////////////////////////////////////////////////////////

// pathFlag implements Flag and defines a string-valued flag.
type pathFlag struct {
	name        string  // The name of the flag.
	description string  // The short-form help text for this flag.
	usage       string  // The long-form help text for this flag.
	s           *string // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *pathFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *pathFlag) Description() string {
	return fmt.Sprintf("%s (default: \"%s\")", f.description, *f.s)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *pathFlag) Usage() string {
	return f.usage
}

// Parse parses the string. This will automatically perform basic tilde expansion on the path.
func (f *pathFlag) Parse(in string) error {
	s, err := homedir.Expand(in)
	if err != nil {
		return err
	}
	*(f.s) = s
	return nil
}

// Path returns a Flag that represents a file path flag with the given name, description, and usage string. It has backing variable s, and default value def.
func Path(name string, s *string, def string, description string, usage string) Flag {
	result := &pathFlag{
		name:        name,
		description: description,
		usage:       usage,
		s:           s,
	}
	*(result.s) = def
	return result
}
