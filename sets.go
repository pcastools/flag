// Sets defines an interface satisfied by sets of flags, and a basic type that implements that interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"fmt"
	"sort"
	"sync"
)

// Set is the interface describing a set of flags
type Set interface {
	Flags() []Flag       // Flags returns the members of the set, in lexicographic order.
	Name() string        // Name returns the name of this collection of flags (or the empty string if not required).
	UsageFooter() string // UsageFooter returns the footer for the usage message for this flag set.
	UsageHeader() string // UsageHeader returns the header for the usage message for this flag set.
	Validate() error     // Validate validates this flag set.
}

/////////////////////////////////////////////////////////////////////////
// BasicSet functions
/////////////////////////////////////////////////////////////////////////

// BasicSet is a basic flag set.
type BasicSet struct {
	name     string       // The name of the flag set
	m        sync.Mutex   // Mutex protecting the following data.
	header   string       // The header text in the usage message (if any).
	footer   string       // The footer text in the usage message (if any).
	flags    flagSlice    // The flags
	validate func() error // The validation function
}

// Add adds the specified flags to s.
func (S *BasicSet) Add(f ...Flag) {
	S.m.Lock()
	defer S.m.Unlock()
	S.flags = append(S.flags, f...)
}

// Flags returns the flags in the set, in lexicographic order. If there are no flags in the set, it returns a non-nil empty slice.
func (S *BasicSet) Flags() []Flag {
	S.m.Lock()
	defer S.m.Unlock()
	result := make(flagSlice, len(S.flags))
	copy(result, S.flags)
	sort.Sort(result)
	return result
}

// Name returns the name of the collection of flags s.
func (S *BasicSet) Name() string {
	if S == nil {
		return ""
	}
	S.m.Lock()
	defer S.m.Unlock()
	return S.name
}

// SetUsageFooter sets the footer text for the usage message for this flag set.
func (S *BasicSet) SetUsageFooter(s string) {
	S.m.Lock()
	defer S.m.Unlock()
	S.footer = s
}

// SetUsageHeader sets the header text for the usage message for this flag set.
func (S *BasicSet) SetUsageHeader(s string) {
	S.m.Lock()
	defer S.m.Unlock()
	S.header = s
}

// UsageFooter returns the footer text in the usage message (if any).
func (S *BasicSet) UsageFooter() string {
	S.m.Lock()
	defer S.m.Unlock()
	return S.footer
}

// UsageHeader returns the header text in the usage message (if any).
func (S *BasicSet) UsageHeader() string {
	S.m.Lock()
	defer S.m.Unlock()
	return S.header
}

// SetValidate sets the validation function.
func (S *BasicSet) SetValidate(f func() error) {
	S.m.Lock()
	defer S.m.Unlock()
	S.validate = f
}

// Validate validates the flag set.
func (S *BasicSet) Validate() error {
	S.m.Lock()
	defer S.m.Unlock()
	return S.validate()
}

// NewBasicSet defines a FlagSet with the given name and flags, and trivial Validate method.
func NewBasicSet(name string, flags ...Flag) *BasicSet {
	fs := make(flagSlice, 0)
	return &BasicSet{
		name:     name,
		flags:    append(fs, flags...),
		validate: func() error { return nil },
	}
}

/////////////////////////////////////////////////////////////////////////
// Operations on flag sets
/////////////////////////////////////////////////////////////////////////

// Merge amalgamates the flag sets in S, returning them as a single flag set with the given name. The usage header and footer messages are concatenated. The Validate method of the returned flag set is obtained by calling s.Validate() for each member s of S in turn and returning the first non-nil result (if any).
func Merge(name string, S ...Set) Set {
	// concatenate the flags
	fs := make(flagSlice, 0)
	for _, s := range S {
		fs = append(fs, s.Flags()...)
	}
	// concatenate the usage headers
	var header string
	for _, s := range S {
		if h := s.UsageHeader(); h != "" {
			if header == "" {
				header = h
			} else {
				header = fmt.Sprintf("%s\n%s", header, h)
			}
		}
	}
	// concatenate the usage footers
	var footer string
	for _, s := range S {
		if f := s.UsageHeader(); f != "" {
			if footer == "" {
				footer = f
			} else {
				footer = fmt.Sprintf("%s\n%s", footer, f)
			}
		}
	}
	// build the validate function
	validate := func() error {
		for _, s := range S {
			if err := s.Validate(); err != nil {
				return err
			}
		}
		return nil
	}
	// assemble the result and return
	return &BasicSet{
		name:     name,
		flags:    fs,
		header:   header,
		footer:   footer,
		validate: validate,
	}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// AddSet adds a given flagset.
func AddSet(s Set) {
	m.Lock()
	defer m.Unlock()
	registeredSets = append(registeredSets, s)
}

// AddSets adds the given flagsets.
func AddSets(S ...Set) {
	m.Lock()
	defer m.Unlock()
	registeredSets = append(registeredSets, S...)
}
