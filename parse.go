// Parse defines functions for flag parsing.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"errors"
	"fmt"
	"os"
)

// ErrHelp is returned if the help flag is specified.
var ErrHelp = errors.New("flag: help requested")

// remainingArgs holds the command-line arguments remaining after all flags have been parsed
var remainingArgs []string

// parsed is true if and only if Parse has been called
var parsed bool

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// allKnownFlags returns all known flags, as a map from the flag names to the corresponding Flags. It returns an error if there is more than one flag with the same name.
func allKnownFlags() (map[string]Flag, error) {
	knownFlags := make(map[string]Flag)
	m.Lock()
	defer m.Unlock()
	for _, s := range registeredSets {
		for _, f := range s.Flags() {
			// First add the name
			name := f.Name()
			if _, ok := knownFlags[name]; ok {
				return nil, fmt.Errorf("duplicate flag: %s", name)
			}
			knownFlags[name] = f
			// Now add the alternative name
			if a, ok := f.(alternativeNamer); ok {
				name := a.AlternativeName()
				if _, ok := knownFlags[name]; ok {
					return nil, fmt.Errorf("duplicate flag: %s", name)
				}
				knownFlags[name] = f
			}
		}
	}
	return knownFlags, nil
}

// readOne examines the known flags and arguments provided and attempts to read one flag from args. It returns whether or not a flag was seen (the boolean success) and, on success, the name and value for the flag, together with the remaining arguments. This is adapted from the function parseOne in the Go standard library [https://golang.org/src/flag/flag.go lines 885--958]. The error ErrHelp is returned if a flag with name 'h' or 'help' is read; in this case value is returned as "" and remaining as nil.
func readOne(knownFlags map[string]Flag, args []string) (success bool, name string, value string, remaining []string, err error) {
	if len(args) == 0 {
		return
	}
	s := args[0]
	if len(s) < 2 || s[0] != '-' {
		remaining = args
		return
	}
	numMinuses := 1
	if s[1] == '-' {
		numMinuses++
	}
	name = s[numMinuses:]
	if len(name) == 0 || name[0] == '-' || name[0] == '=' {
		err = fmt.Errorf("bad flag syntax: %s", s)
		name = ""
		return
	}

	// It's a flag. Does it have an argument?
	args = args[1:]
	hasValue := false
	for i := 1; i < len(name); i++ { // equals cannot be first
		if name[i] == '=' {
			value = name[i+1:]
			hasValue = true
			name = name[0:i]
			break
		}
	}

	// Is it a help flag?
	if name == "help" || name == "h" {
		success = true
		value = ""
		remaining = nil
		err = ErrHelp
		return
	}

	// Do we know this flag?
	f, ok := knownFlags[name]
	if !ok {
		err = fmt.Errorf("flag provided but not defined: -%s", name)
		name = ""
		return
	}

	// Is it a boolean Flag?
	if fb, ok := f.(isBooleaner); ok && fb.IsBoolean() { // special case: doesn't need an arg
		if !hasValue {
			value = "true"
		}
		success = true
		remaining = args
		return
	}

	// The flag must have a value, which might be the next argument.
	if !hasValue && len(args) > 0 {
		// value is the next arg
		hasValue = true
		value, args = args[0], args[1:]
	}
	if !hasValue {
		err = fmt.Errorf("flag needs an argument: -%s", name)
		name = ""
	}
	success = true
	remaining = args
	return

}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Parse parses flags and command-line arguments. It then calls Validate for each registered flag set in turn. On parse or validation error, Parse prints the error and a usage message to os.Stderr and exits (via os.Exit) with a non-zero error code. If the help flag (-h or -help) is provided then Parse prints a usage message to os.Stderr and exits (via os.Exit) with error code zero. For more control of behaviour on error, see ParseWithErr. Exactly one of Parse and ParseWithErr should be called, after all flags and sets have been registered.
func Parse() {
	if err := ParseWithErr(); err == ErrHelp {
		Usage()
		os.Exit(0)
	} else if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing command-line flags: %s\n", err)
		os.Exit(1)
	}
}

// ParseWithErr parses flags and command-line arguments. It then calls Validate for each registered flag set in turn. It returns ErrHelp if the help flag (-h or -help) was present. Exactly one of Parse and ParseWithErr should be called, after all flags and sets have been registered.
func ParseWithErr() error {
	if parsed {
		return errors.New("parse called twice")
	}
	parsed = true
	// grab the command-line arguments, removing the command name
	remainingArgs = make([]string, len(os.Args)-1)
	copy(remainingArgs, os.Args[1:])
	// grab the flags from each flag set
	knownFlags, err := allKnownFlags()
	if err != nil {
		// we found more than one flag with the same name
		return err
	}
	// record the flags that we see
	todo := make(map[string]string)
	var name, value string
	var ok bool
	for len(remainingArgs) > 0 {
		ok, name, value, remainingArgs, err = readOne(knownFlags, remainingArgs)
		if err != nil {
			return err
		} else if !ok {
			// The first argument wasn't a flag, so the remaining arguments are non-flag arguments
			break
		}
		// record the name and value for the flag that we found
		todo[name] = value
	}
	// parse the flags
	for f, v := range todo {
		if err := knownFlags[f].Parse(v); err != nil {
			return fmt.Errorf("%s: %w", f, err)
		}
	}
	// validate the flag sets
	for _, s := range registeredSets {
		if err := s.Validate(); err != nil {
			return err
		}
	}
	return nil
}

// Args returns the non-flag arguments.
func Args() []string {
	return remainingArgs
}

// NArg is the number of arguments remaining after flags have been processed.
func NArg() int {
	return len(remainingArgs)
}

// Arg returns the i-th command-line argument. Arg(0) is the first remaining argument after flags have been processed. Arg returns an empty string if the requested element does not exist.
func Arg(i int) string {
	if i >= len(remainingArgs) || i < 0 {
		return ""
	}
	return remainingArgs[i]
}
