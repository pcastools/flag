// Usage defines a function that prints a usage message to os.Stderr.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"fmt"
	"github.com/fatih/color"
	"github.com/mitchellh/go-wordwrap"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"strings"
	"text/tabwriter"
)

// Usage prints a usage message to os.Stderr.
func Usage() {
	m.Lock()
	defer m.Unlock()
	// Get the terminal width
	width, _, err := terminal.GetSize(0)
	if err != nil {
		width = 80
	}
	// Get the bold escape sequence
	bold := color.New(color.Bold)
	// Set up a tabwriter, which we will use to print the flag descriptions
	tw := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	// Print the global header
	if len(globalHeader) != 0 {
		header := wordwrap.WrapString(globalHeader, uint(width))
		header = strings.ReplaceAll(header, "\nUsage:", "\n"+bold.Sprint("Usage")+":")
		fmt.Fprintf(os.Stderr, "%s\n\n", header)
	}
	// Output the usage text for each flag set, in order
	for _, S := range registeredSets {
		// Print the title of the flag set in bold, if it is set
		if title := S.Name(); title != "" {
			fmt.Fprintf(os.Stderr, "%s\n", bold.Sprint(title))
		}
		// Print the header
		if header := S.UsageHeader(); header != "" {
			fmt.Fprintf(os.Stderr, "%s\n\n", wordwrap.WrapString(header, uint(width)))
		}
		// Print the flag descriptions
		if fs := S.Flags(); len(fs) > 0 {
			for _, f := range fs {
				name := "-" + f.Name()
				if a, ok := f.(alternativeNamer); ok {
					name += ", -" + a.AlternativeName()
				}
				fmt.Fprintf(tw, "  %s\t%s\n", name, f.Description())
			}
			tw.Flush()
			fmt.Fprintf(os.Stderr, "\n")
		}
		// Print the flag usage messages, if any
		for _, f := range S.Flags() {
			if usage := f.Usage(); usage != "" {
				fmt.Fprintf(os.Stderr, "%s\n\n", wordwrap.WrapString(usage, uint(width)))
			}
		}
		// Print the footer
		if footer := S.UsageFooter(); footer != "" {
			fmt.Fprintf(os.Stderr, "%s\n\n", wordwrap.WrapString(footer, uint(width)))
		}
	}
	// Print the global footer
	if globalFooter != "" {
		fmt.Fprintf(os.Stderr, "%s\n\n", wordwrap.WrapString(globalFooter, uint(width)))
	}
}

// SetGlobalHeader sets the global header for the usage message.
func SetGlobalHeader(s string) {
	m.Lock()
	defer m.Unlock()
	globalHeader = s
}

// SetGlobalFooter sets the global footer for the usage message.
func SetGlobalFooter(s string) {
	m.Lock()
	defer m.Unlock()
	globalFooter = s
}
