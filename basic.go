// Basic defines basic types that satisfy the Flag interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package flag

import (
	"encoding"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// boolFlag functions
/////////////////////////////////////////////////////////////////////////

// boolFlag implements Flag and defines a boolean-valued flag.
type boolFlag struct {
	name        string // The name of the flag.
	description string // The short-form help text for this flag.
	usage       string // The long-form help text for this flag.
	b           *bool  // A pointer to the variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *boolFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *boolFlag) Description() string {
	return fmt.Sprintf("%s (default: %v)", f.description, *f.b)
}

// Usage returns long-form usage text (or the empty string if no such usage text is set).
func (f *boolFlag) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *boolFlag) Parse(in string) error {
	val, err := strconv.ParseBool(strings.TrimSpace(in))
	if err != nil {
		return err
	}
	*(f.b) = val
	return nil
}

// IsBoolean indicates that this flag is a boolean flag in the sense of the flag package.
func (f *boolFlag) IsBoolean() bool {
	return true
}

// Bool returns a Flag that represents a boolean flag with the given name, description, usage string, backing variable, and default value.
func Bool(name string, b *bool, def bool, description string, usage string) Flag {
	result := &boolFlag{
		name:        name,
		description: description,
		usage:       usage,
		b:           b,
	}
	*(result.b) = def
	return result
}

/////////////////////////////////////////////////////////////////////////
// intFlag functions
/////////////////////////////////////////////////////////////////////////

// intFlag implements Flag and defines an integer-valued flag.
type intFlag struct {
	name        string // The name of the flag.
	description string // The short-form help text for this flag.
	usage       string // The long-form help text for this flag.
	n           *int   // A pointer to the variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *intFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *intFlag) Description() string {
	return fmt.Sprintf("%s (default: %d)", f.description, *f.n)
}

// Usage returns long-form usage text (or the empty string if no such usage text is set).
func (f *intFlag) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *intFlag) Parse(in string) error {
	val, err := strconv.Atoi(in)
	if err != nil {
		return err
	}
	*(f.n) = val
	return nil
}

// Int returns a Flag that represents an integer flag with the given name, description, usage string, backing variable, and default value.
func Int(name string, n *int, def int, description string, usage string) Flag {
	result := &intFlag{
		name:        name,
		description: description,
		usage:       usage,
		n:           n,
	}
	*(result.n) = def
	return result
}

/////////////////////////////////////////////////////////////////////////
// stringFlag functions
/////////////////////////////////////////////////////////////////////////

// stringFlag implements Flag and defines a string-valued flag.
type stringFlag struct {
	name        string  // The name of the flag.
	description string  // The short-form help text for this flag.
	usage       string  // The long-form help text for this flag.
	s           *string // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *stringFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *stringFlag) Description() string {
	return fmt.Sprintf("%s (default: \"%s\")", f.description, *f.s)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *stringFlag) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *stringFlag) Parse(in string) error {
	*(f.s) = in
	return nil
}

// String returns a Flag that represents a string-valued flag with the given name, description, and usage string. It has backing variable s, and default value def.
func String(name string, s *string, def string, description string, usage string) Flag {
	result := &stringFlag{
		name:        name,
		description: description,
		usage:       usage,
		s:           s,
	}
	*(result.s) = def
	return result
}

/////////////////////////////////////////////////////////////////////////
// durationFlag functions
/////////////////////////////////////////////////////////////////////////

// durationFlag implements Flag and defines a time.Duration-valued flag.
type durationFlag struct {
	name        string         // The name of the flag.
	description string         // The short-form help text for this flag.
	usage       string         // The long-form help text for this flag.
	d           *time.Duration // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *durationFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *durationFlag) Description() string {
	return fmt.Sprintf("%s (default: %s)", f.description, *f.d)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *durationFlag) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *durationFlag) Parse(in string) error {
	result, err := time.ParseDuration(in)
	if err != nil {
		return err
	}
	*(f.d) = result
	return nil
}

// Duration returns a Flag that represents a time.Duration-valued flag with the given name, description, and usage string. It has backing variable d, and default value def.
func Duration(name string, d *time.Duration, def time.Duration, description string, usage string) Flag {
	result := &durationFlag{
		name:        name,
		description: description,
		usage:       usage,
		d:           d,
	}
	*(result.d) = def
	return result
}

/////////////////////////////////////////////////////////////////////////
// helpFlag functions
/////////////////////////////////////////////////////////////////////////

// helpFlag is a trivial implementation of Flag, used to display the -h/-help flag in the usage message.
type helpFlag struct{}

// Name returns the flag name (without leading hyphen).
func (helpFlag) Name() string {
	return "h"
}

// AlternativeName returns the alternative flag name (without leading hyphen).
func (helpFlag) AlternativeName() string {
	return "help"
}

// Description returns a one-line description of this variable.
func (helpFlag) Description() string {
	return "Display this usage message and exit"
}

// Usage returns long-form usage text (or the empty string if not set).
func (helpFlag) Usage() string {
	return ""
}

// Parse parses the string.
func (helpFlag) Parse(_ string) error {
	return nil
}

/////////////////////////////////////////////////////////////////////////
// stringFlagNoDefault functions
/////////////////////////////////////////////////////////////////////////

// stringFlagNoDefault implements flag.Flag and defines a string-valued flag.
type stringFlagNoDefault struct {
	name        string  // The name of the flag.
	description string  // The short-form help text for this flag.
	usage       string  // The long-form help text for this flag.
	s           *string // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *stringFlagNoDefault) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *stringFlagNoDefault) Description() string {
	return f.description
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *stringFlagNoDefault) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *stringFlagNoDefault) Parse(in string) error {
	*(f.s) = in
	return nil
}

// StringNoDefault returns a Flag that represents a string-valued flag with the given name, description, and usage string. It has backing variable s.
func StringNoDefault(name string, s *string, description string, usage string) Flag {
	return &stringFlagNoDefault{
		name:        name,
		description: description,
		usage:       usage,
		s:           s,
	}
}

/////////////////////////////////////////////////////////////////////////
// funcFlag functions
/////////////////////////////////////////////////////////////////////////

// funcFlag implements Flag and defines a func-valued flag.
type funcFlag struct {
	name        string             // The name of the flag.
	description string             // The short-form help text for this flag.
	usage       string             // The long-form help text for this flag.
	fn          func(string) error // The function called to parse this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *funcFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *funcFlag) Description() string {
	return f.description
}

// Usage returns long-form usage text (or the empty string if no such usage text is set).
func (f *funcFlag) Usage() string {
	return f.usage
}

// Parse parses the flag.
func (f *funcFlag) Parse(in string) error {
	return f.fn(in)
}

// Func returns a Flag with the given name, description, and usage string. Each time the flag is seen, fn is called with the value of the flag. If fn returns a non-nil error, it will be treated as a flag value parsing error.
func Func(name string, description string, usage string, fn func(string) error) Flag {
	return &funcFlag{
		name:        name,
		description: description,
		usage:       usage,
		fn:          fn,
	}
}

/////////////////////////////////////////////////////////////////////////
// textVarFlag functions
/////////////////////////////////////////////////////////////////////////

// textVarFlag implements Flag and defines a TextVar flag.
type textVarFlag struct {
	name        string                   // The name of the flag.
	description string                   // The short-form help text for this flag.
	usage       string                   // The long-form help text for this flag.
	p           encoding.TextUnmarshaler // The backing value pointer.
}

// Name returns the flag name (without leading hyphen).
func (f *textVarFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *textVarFlag) Description() string {
	if m, ok := f.p.(encoding.TextMarshaler); ok {
		if b, err := m.MarshalText(); err == nil {
			return fmt.Sprintf("%s (default: %s)", f.description, string(b))
		}
	}
	return f.description
}

// Usage returns long-form usage text (or the empty string if no such usage text is set).
func (f *textVarFlag) Usage() string {
	return f.usage
}

// Parse parses the flag.
func (f *textVarFlag) Parse(in string) error {
	return f.p.UnmarshalText([]byte(in))
}

// TextVar returns a Flag with the given name, description, default value, and usage string. The argument p must be a pointer to a variable that will hold the value of the flag, and p must implement encoding.TextUnmarshaler. If the flag is used, the flag value will be passed to p's UnmarshalText method. The type of the default value must be the same as the type of p.
func TextVar(name string, p encoding.TextUnmarshaler, value encoding.TextMarshaler, description string, usage string) Flag {
	// The following code is copied from the standard flag library.
	ptrVal := reflect.ValueOf(p)
	if ptrVal.Kind() != reflect.Ptr {
		panic("variable value type must be a pointer")
	}
	defVal := reflect.ValueOf(value)
	if defVal.Kind() == reflect.Ptr {
		defVal = defVal.Elem()
	}
	if defVal.Type() != ptrVal.Type().Elem() {
		panic(fmt.Sprintf("default type does not match variable type: %v != %v", defVal.Type(), ptrVal.Type().Elem()))
	}
	ptrVal.Elem().Set(defVal)
	return &textVarFlag{
		name:        name,
		description: description,
		usage:       usage,
		p:           p,
	}
}
